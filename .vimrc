set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab

let &path.="src/include,/usr/include/,"

let g:ycm_enable_diagnostic_highlighting = 0
let g:ycm_enable_diagnostic_signs = 0

set makeprg=cd\ build\ &&\ cmake\ ..\ &&\ make\ -j4
nnoremap <F4> :wall<cr>:make!<cr>
nnoremap <F5> :!./build/ARSonist<cr>

nnoremap <F3> :source\ .session.vim<cr>
nnoremap <F2> :mksession!\ .session.vim<cr>

## ARSonist
### An ARS testing utility

This utility reads sensory logs taken from a robot and compares them to an orientation estimate made with ARS (Angular Radon Spectrum) to verify the latter's precision.

### Building

In order to build ARSonist, you will need:

* Cmake
* Make/g++ or equivalent

As well as the following C++ libraries installed on your system:

* Boost
* Eigen3
* OpenMP
* [ars](https://www.github.com/dlr1516/ars)

Once these dependencies are satisfied, build as you would normally. Assuming you just cloned the project from this repository:

```{bash}
cd arsonist
mkdir build
cd build
cmake ..
make
```

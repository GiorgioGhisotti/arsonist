/** ARSonist
 *                                                )
 *    ,%,                                     ) _(___[]_
 *    %%%,&&&,                     ,%%,      (;`       /\
 *    %Y/%&&&&                     %%%%   ___/_____)__/ _\__     ,%%,
 *  ^^^||^&\Y&^^^^^^^^^^^^^^^^^^^^^%Y/%^^/ (_()   (  | /____/\^^^%%%%^^
 *    `    || _,..=xxxxxxxxxxxx,    ||   |(' |LI (.)I| | LI ||   %\Y%
 *   -=      /L_Y.-"""""""""`,-n-. `    @'---|__||___|_|____||_   ||
 *  ___-=___.--'[========]|L]J: []\ __________@//@___________) )______
 * -= _ _ _ |/ _ ''_ " " ||[ -_ 4 |  _  _  _  _  _  _  _  _  _  _  _
 *          '-(_)-(_)----'v'-(_)--'
 * --------------------------------------------------------------------
 *
 */
#include "Matchbox.h"

namespace matchbox {

	void makeMatch(std::vector<double> ars_feature, double x, double y, double theta_reference, ars::Vector2Vector points, matchbox::match& out){
		out.ars_feature = ars_feature;
		out.x = x;
		out.y = y;
		out.theta_reference = mod180(theta_reference);
		out.theta_reference_raw = theta_reference*180/M_PI;
		out.points = points;
	}
	
	Matchbox::Matchbox() {
	}

	Matchbox::Matchbox(bool v) {
		this->setVerbose(v);
	}
	
	/*
	 * Add given matches to the object's cache and assign neighbours to each other.
	 */
	void Matchbox::addMatches(std::vector<match>& new_matches){
		int index = 0;
		for(match m : new_matches){
			match * new_match = new match();
			new_match->x = m.x;
			new_match->y = m.y;
			new_match->theta_reference = m.theta_reference;
			new_match->theta_reference_raw = m.theta_reference_raw;
			new_match->ars_feature = m.ars_feature;
			new_match->points = m.points;

#pragma omp parallel for	//hopefully this is safe...
			for(int i = 0; i<this->matches.size(); i++){	//add match to compare vector of nearby matches
				match* n = this->matches[i];
				double dist = abs(sqrt(pow(n->x - new_match->x,2) + pow(n->y - new_match->y,2)));
				if(dist < 0.5 && abs(n->theta_reference_raw - new_match->theta_reference_raw) <= 90 && n->neighbours.size() < MAX_NEIGHBOURS){
					n->neighbours.push_back(new_match);
				}
			}
			this->matches.push_back(new_match);
			index++;
		}
	}
	
	/**
	 * compute ars angle and compare nearby points to determine their orientation relative to each other.
	 * then, compare that orientation to the reference angles.
	 */
	void Matchbox::computeAccuracy(){
#pragma omp parallel for
		for(int i = 0; i<this->matches.size(); i++)
			this->computeAngle(this->matches[i]);

		for(match* m : this->matches){
			double err = 0;
			double diff = 0;
			double corr_err = 0;
			int i = 0;
			int nb_corr = 0;
			for(match* n : m->neighbours){
				double ref = subAngles(n->theta_reference, m->theta_reference);
				err += subAngles(ref, subAngles(n->ars_angle, m->ars_angle));
				diff += ref;
				if(subAngles(ref, m->correlation_deltas[i]) < THRESHOLD){
					corr_err += subAngles(ref, m->correlation_deltas[i]);
					nb_corr++;
				} else{
					this->outliers++;
					m->outlier = true;
				}
				this->corr_count++;
				i++;
			}
			
			int nb = m->neighbours.size();
			if(nb > 0){
				err = err/(double) nb;
				diff = diff/(double) nb;
				err = std::min(err, 180-err);
				diff = std::min(diff, 180-diff);
				this->tested++;
			}

			if(nb_corr > 0){
				corr_err = corr_err/(double) nb_corr;
				corr_err = std::min(corr_err, 180-corr_err);
			}
			
			this->delta.push_back(corr_err);

			m->delta = err;
			m->corr_delta_avg = corr_err;
			m->theta_reference_delta = diff;
			this->delta_percent.push_back(100*err);
		}
		
		double avgdelta = 0.0;
		if(this->verbose) std::cout << "\nWorking out deltas...\n";
		for(double d : this->delta){
			avgdelta += abs(d);
		}
		if(this->tested > 0) std::cout << "\nAverage delta: " << avgdelta/(double) this->tested << "°\nNumber of outliers: " << this->outliers << " out of " << corr_count << " ars correlations\n" ;
		else std::cout << "No points are close enough for comparison.\n";
	}
	
	/**
	 * compute point angles using ARS
	 */
	void Matchbox::computeAngle(match* m){
		ars::FourierOptimizerBB1D optim(m->ars_feature);
		double xopt, ymin, ymax;
		optim.findGlobalMax(0,M_PI,xopt,ymin,ymax);
	
		m->ars_angle = mod180(xopt);

		std::vector<double> cd(m->neighbours.size());

#pragma omp parallel for
		for(int i=0; i<m->neighbours.size(); i++){
			match* neighbour = m->neighbours[i];
			std::vector<double> corr;
			ars::computeFourierCorr(m->ars_feature, neighbour->ars_feature, corr);
			double d;
			ars::FourierOptimizerBB1D fo(corr);
			fo.findGlobalMax(0, M_PI, d, ymin, ymax);
			cd[i] = std::min(mod180(d), abs(180 - mod180(d)));
		}
		m->correlation_deltas = cd;
	}

	/*
	 * Plot ars angles, reference angles and the delta between them using gnuplot
	 */
	void Matchbox::plotDelta(int width, int height, std::string out_file){

		if(verbose) std::cout << "Plotting data..." << std::flush;
		Gnuplot gp("gnuplot -persist");
		gp << "reset\nset terminal svg size " << this->matches.size() * 5 << "," << std::max(720, int(this->matches.size()/8)) << "dynamic" << std::endl;
		gp << "set output \"plots/delta.svg\"\n";
		gp << "plot '-' title \"ars\" w p, '-' title \"ref\" w p, '-' title \"delta\" w i\n";
		if(verbose) std::cout << "-ars angles-\n";
		for(int i=0; i<this->matches.size(); i++) gp << i << " " << 180 - this->matches[i]->ars_angle << std::endl;
		gp << "e\n";
		if(verbose) std::cout << "-reference angles-\n";
		for(int i=0; i<this->matches.size(); i++) gp << i << " " << this->matches[i]->theta_reference << std::endl;
		gp << "e\n";
		if(verbose) std::cout << "-deltas-\n";
		for(int i=0; i<this->delta.size(); i++) gp << i << " " << this->delta[i] << std::endl;
		gp << "e\n";
		gp << "quit\n";
		gp.do_flush();

	}

	/*
	 * Plot the laser scans of outliers together with their neighbours' scans using gnuplot
	 */
	void Matchbox::plotOutliers(int width, double threshold, std::string out_dir){
		bool b = false;
#pragma omp parallel for 
		for(int i = 0; i < this->matches.size(); i++){
			matchbox::match* m = this->matches[i];
			if(m->outlier){
				std::stringstream plot_file;
				plot_file << out_dir << "/outlier_" << i << "_[" << std::setprecision(3) << std::fixed << m->x << "]x-[" << std::setprecision(3) << std::fixed << m->y << "]y-delta[" << std::setprecision(3) << std::fixed << m->delta << "°]-ref_delta[" << std::setprecision(3) << std::fixed << m->theta_reference_delta << "°].png";
				if(exists(plot_file.str())) continue;
				b=true;

				int cols = 80;
				int lines = 24;
#ifdef TIOCGSIZE
				struct ttysize ts;
				ioctl(STDIN_FILENO, TIOCGSIZE, &ts);
				cols = ts.ts_cols;
				lines = ts.ts_lines;
#elif defined(TIOCGWINSZ)
				struct winsize ts;
				ioctl(STDIN_FILENO, TIOCGWINSZ, &ts);
				cols = ts.ws_col;
				lines = ts.ws_row;
#endif /* TIOCGSIZE */

				std::stringstream stream;
				stream << "\r" << std::setfill(' ') << std::setw(cols) << std::flush << "\rRendering plot for outlier in "<< "[" << std::setprecision(3) << std::fixed << m->x << "," << std::setprecision(3) << std::fixed << m->y << "]";
				std::cout << stream.str() << std::flush;
				Gnuplot gp2("gnuplot");
				gp2 << "reset\nset terminal png size " << width << "," << width << "\n";
				gp2 << "set xrange [0:80]\n";
				gp2 << "set yrange [-40:40]\n";
				gp2 << "set output \"" << plot_file.str() << "\"\n";
				gp2 << "plot '-' title \"outlier ars: " << std::setprecision(3) << std::fixed << m->ars_angle << " ref: " << std::setprecision(3) << std::fixed << m->theta_reference << "\" w p";
				for(int i = 0; i<m->neighbours.size(); i++)	gp2 << ", '-' title \"neighbour " << i << " ars: " << std::setprecision(3) << std::fixed  << m->neighbours[i]->ars_angle << " correlation delta: " << std::setprecision(3) << std::fixed << m->correlation_deltas[i] <<  " ref: "<< std::setprecision(3) << std::fixed  << m->neighbours[i]->theta_reference << "\" w p";
				gp2 << "\n";
				for(ars::Vector2 point : m->points) gp2 << (double)point[0] << " " << (double)point[1]  << "\n";
				gp2 << "e\n";
				for(int i = 0; i<m->neighbours.size(); i++){
					for(ars::Vector2 point : m->neighbours[i]->points) gp2 << (double)point[0] << " " << (double)point[1] << "\n";
					gp2 << "e\n";
				}
				gp2.do_flush();
			}
		}
		if(b) std::cout << "\n";
	}

	/*
	 * Plot the Hough spectrum of each point's laser scan using gnuplot
	 */
	void Matchbox::plotHough(int width, int height, std::string out_dir){
		bool b = false;
#pragma omp parallel for
		for(int i = 0; i<this->matches.size(); i++){
			boost::filesystem::create_directories(out_dir);
			matchbox::match* m = this->matches[i];
			std::stringstream plot_file;
			plot_file << out_dir << "/hough_" << i << "_[" << std::setprecision(3) << std::fixed << m->x << "]x-[" << std::setprecision(3) << std::fixed << m->y << "]y.png";
			if(exists(plot_file.str())) continue;
			b=true;

			ars::HoughSpectrum* hough;
			hough = new ars::HoughSpectrum();
			hough->insertPoint(m->points.begin(), m->points.end());

			int cols = 80;
			int lines = 24;
#ifdef TIOCGSIZE
			struct ttysize ts;
			ioctl(STDIN_FILENO, TIOCGSIZE, &ts);
			cols = ts.ts_cols;
			lines = ts.ts_lines;
#elif defined(TIOCGWINSZ)
			struct winsize ts;
			ioctl(STDIN_FILENO, TIOCGWINSZ, &ts);
			cols = ts.ws_col;
			lines = ts.ws_row;
#endif /* TIOCGSIZE */

			std::stringstream stream;
			stream << "\r" << std::setfill(' ') << std::setw(cols) << std::flush << "\rRendering Hough spectrum plot for point in "<< "[" << std::setprecision(3) << std::fixed << m->x << "," << std::setprecision(3) << std::fixed << m->y << "]";
			std::cout << stream.str() << std::flush;
			Gnuplot gp2("gnuplot");
			gp2 << "reset\nset terminal svg size " << width << "," << height << "\n";
			gp2 << "set output \"" << plot_file.str() << "\"\n";
			gp2 << "plot '-' title \"Hough\" w l\n";
			int a = 0;
			std::vector<double> sp(hough->spectrum().data(), hough->spectrum().data() + hough->spectrum().rows() * hough->spectrum().cols()); 
			for(double bin : sp) gp2 << a++ << " " << bin << "\n";
			gp2 << "e\n";
			gp2.do_flush();
			delete hough;
		}
		if(b) std::cout << "\n";
	}

	double mod180(double angle){
		return 180/M_PI*(angle - std::floor(angle / M_PI) * M_PI);
	}

	//Correctly subtract angles
	double subAngles(double a, double b){
		return std::min(abs(a-b), abs(180 - abs(a-b)));
	}

	void Matchbox::setVerbose(bool v){
		this->verbose = v;
	}

	//Check if file exists
	bool exists (const std::string& name) {
		struct stat buffer;   
		return (stat (name.c_str(), &buffer) == 0); 
	}

}

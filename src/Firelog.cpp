/** ARSonist
 *                                                )
 *    ,%,                                     ) _(___[]_
 *    %%%,&&&,                     ,%%,      (;`       /\
 *    %Y/%&&&&                     %%%%   ___/_____)__/ _\__     ,%%,
 *  ^^^||^&\Y&^^^^^^^^^^^^^^^^^^^^^%Y/%^^/ (_()   (  | /____/\^^^%%%%^^
 *    `    || _,..=xxxxxxxxxxxx,    ||   |(' |LI (.)I| | LI ||   %\Y%
 *   -=      /L_Y.-"""""""""`,-n-. `    @'---|__||___|_|____||_   ||
 *  ___-=___.--'[========]|L]J: []\ __________@//@___________) )______
 * -= _ _ _ |/ _ ''_ " " ||[ -_ 4 |  _  _  _  _  _  _  _  _  _  _  _
 *          '-(_)-(_)----'v'-(_)--'
 * --------------------------------------------------------------------
 *
 */
#include "Firelog.h"

namespace firelog{
	
	Firelog::Firelog(){

	}

	/**
	 * Initializes object parameters with values from config file cfgFile
	 */
	Firelog::Firelog(std::string cfg_path, std::string log_path, bool v, bool safe){
		std::string ext = log_path.substr(log_path.length() - 4, 4);
		this->setVerbose(v);
		this->safe_params = safe;
		if(ext.compare(".bin") == 0){
			this->initE80(cfg_path, log_path, v);
			return;
		}

		setNum();
		setAngles();

		CarmenV2Reader reader;

		try{
			reader.readCarmen(log_path);
		} catch (int e) {
			std::cout << "Error reading parameters from file!" << std::endl;
			exit(2);
		}

		this->messes = reader.scans;

		for(auto& mess : this->messes){
			this->getPoints(mess.scan.ranges);
		}
	}

	void Firelog::setNum(int num){
		this->num = num;
	}

	void Firelog::setAngles(double angleMin, double angleRes){
		this->angleMin = angleMin;
		this->angleRes = angleRes;
	}

	void Firelog::getPoints(std::vector<double>& ranges){
		ars::Vector2Vector points;
		try{
			std::cout << "\r|" << std::flush;
			rangeToPoint(ranges, points, this->maxRange);
			std::cout << "\r-" << std::flush;
		} catch (int e) {
			std::cout << "Could not convert range data to points!" << std::endl;
			exit(2);
		}
		this->scans_points.push_back(points);
	}

	/** \brief Converts an array of ranges to a 4x32 aligned vector of points.
	 */
	void Firelog::rangeToPoint(std::vector<double>& ranges, ars::Vector2Vector& points, double& max_range){
		Eigen::Vector2d p;
		for (int i=0; i<this->num; ++i){
			double a = this->angleMin + this->angleRes * i;
			p << ranges[i] * cos(a), ranges[i] * sin(a);
			if(ranges[i] < max_range) points.push_back(p);
		}
		points.push_back(ars::Vector2::Zero());
	}

	void writeCache(std::vector<double> ars, std::fstream& cache){
		for(int i = 0; i < ars.size(); i++){
			if(i!=0) cache << " ";
			cache << ars[i];
		}
		cache << std::endl;
	}

	void readCache(std::vector<double>& ars, std::fstream& cache){
		std::string line;
		std::getline(cache, line);
		std::vector<std::string> coeffs_str;
		std::vector<double> coeffs;
		boost::split(coeffs_str, line, boost::is_any_of(" "), boost::token_compress_on);
		for (std::string s : coeffs_str){
			std::string::size_type sz;
		   	if(!s.empty()) coeffs.push_back(std::stod(s, &sz));
		}
		ars = coeffs;
	}

	void Firelog::setVerbose(bool v){
		this->verbose = v;
	}

	void Firelog::setMaxRange(double r){
		this->maxRange = r;
	}

// {{{1 E80 log reading stuff
	void Firelog::initE80(std::string cfg_path, std::string log_path, bool v){
		std::cout << "Reading AGV log...\n";
		int msg_size = 24411;
		int msg_type = 0;
		double scanSickOffset = -0.19;
		this->num = 1440;
		this->angleRes = 0.25;
		this->angleMin = 0.0;

		if(!this->safe_params){
			this->params.read(cfg_path);
			this->params.getParam<double>("scanAngleMinDeg", this->angleMin, double(0.0));
			this->params.getParam<double>("scanAngleIncDeg", this->angleRes, double(0.25));
			this->params.getParam<int>("scanBeamNum", this->num, int(1440));
			this->params.getParam<int>("messSize", msg_size, int(24411));
			this->params.getParam<int>("messType", msg_type, 0);
			this->params.getParam<double>("scanSickOffsetDeg", scanSickOffset, double(-0.19));
		}

		this->angleMin *= M_PI/180.0;
		this->angleRes *= M_PI/180.0;
		scanSickOffset *= M_PI/180.0;
		
		LaserScan scan(this->angleMin, this->num*this->angleRes, this->num);
		
		std::ifstream log;
		log.open(log_path, std::ios::binary);
		if(!log.is_open()){
			std::cout << "Error opening log file :(\n";
			exit(2);
		}

		AGV_Message_V2 msg;
		while(!log.eof()){
			std::cout << "\r/" << std::flush;
			RobotLaserMess mess;
			log.read((char*) &msg, msg_size);
			extractFromMsgLaserScan(msg, msg_type, this->num, this->angleMin, this->angleRes, scanSickOffset, scan);
			mess.scan = scan;
			std::cout << "\r\\" << std::flush;
			extractFromMsgAgvPose(msg, mess.wTb, mess.wTl);
			this->messes.push_back(mess);
		}
		for(auto& mess : this->messes){
			this->getPoints(mess.scan.ranges);
		}
		std::cout << "\n";
		std::cout << "Read " << this->messes.size() << " points!\n";
	}

    void extractFromMsgLaserScan(const AGV_Message_V2& mess, int messType, int scanNumBeams, double scanAngleMin, double scanAngleInc, double scanSickOffset, LaserScan& scan) {
        // mess.scan_start_angle is in mdeg (1 mdeg = 0.001 deg)
        // Internal offset of Sick Laser Scanners must also be taken into account!!!
        double messAngleMin;

        if (messType == MESS_NAV350) {
            messAngleMin = mess.scan_start_angle * M_PI / 180000.0 + scanSickOffset;
        } else if (messType == MESS_R2000) {
            messAngleMin = mess.scan_start_angle * M_PI / 18000.0;
        } else {
            std::cerr << __FILE__ << "," << __LINE__ << ": invalid message type " << messType << std::endl;
            exit(-1);
        }

        // Reads timestamp from message
        double nav_elab_timestamp = mess.time.nav_timestamp & 4095;
        double abs_timestamp = mess.time.agv_epoch_time * 1000.0 + mess.time.agv_epoch_millisec;
        double scan_timestamp = mess.time.scan_timestamp & 4095;
        if (scan_timestamp > nav_elab_timestamp) {
            scan_timestamp -= 4096;
        }
        double scanTimestampComplete = ((scan_timestamp - nav_elab_timestamp + abs_timestamp) / 1000.0);

        scan.setNumBeams(scanNumBeams);
        scan.setAngleMin(messAngleMin);
        scan.setAngleInc(scanAngleInc);
        scan.setLaserFoV(scanNumBeams * scanAngleInc);
        scan.setTimestamp(scanTimestampComplete);

        scan.ranges.resize(scanNumBeams);
        scan.points.resize(scanNumBeams);

        for (int i = 0; i < scan.getNumBeams(); ++i) {
            scan.ranges[i] = 0.001 * mess.points[i].rho;
            double rho = scan.ranges[i];
            double alpha = i * scan.getAngleInc() + messAngleMin;
            scan.points[i] = Eigen::Vector2d(rho * cos(alpha), rho * sin(alpha));
        }
    }

    void extractFromMsgAgvPose(const AGV_Message_V2& mess, Eigen::Vector3d& wTv, Eigen::Vector3d& vTl) {
        Eigen::Vector3d sensorVec(mess.sensor_x / 1000.0, mess.sensor_y / 1000.0, mess.sensor_h * M_PI / 18000.0);
        Eigen::Vector3d agvVec(mess.agv_x / 1000.0, mess.agv_y / 1000.0, mess.agv_h * M_PI / 18000.0);
		wTv = agvVec;
		vTl = sensorVec;
    }
// }}}1
}

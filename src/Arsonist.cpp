/** ARSonist
 *                                                )
 *    ,%,                                     ) _(___[]_
 *    %%%,&&&,                     ,%%,      (;`       /\
 *    %Y/%&&&&                     %%%%   ___/_____)__/ _\__     ,%%,
 *  ^^^||^&\Y&^^^^^^^^^^^^^^^^^^^^^%Y/%^^/ (_()   (  | /____/\^^^%%%%^^
 *    `    || _,..=xxxxxxxxxxxx,    ||   |(' |LI (.)I| | LI ||   %\Y%
 *   -=      /L_Y.-"""""""""`,-n-. `    @'---|__||___|_|____||_   ||
 *  ___-=___.--'[========]|L]J: []\ __________@//@___________) )______
 * -= _ _ _ |/ _ ''_ " " ||[ -_ 4 |  _  _  _  _  _  _  _  _  _  _  _
 *          '-(_)-(_)----'v'-(_)--'
 * --------------------------------------------------------------------
 *
 */

// {{{1 Includes
#include <iostream>
#include <iomanip>

#include <sys/ioctl.h>

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <fstream>
#include <string>

#include <ars/ars2d.h>
#include <ars/BBOptimizer1d.h>
#include <ars/functions.h>
#include <ars/definitions.h>
#include <ars/HoughSpectrum.h>
#include <omp.h>
#include <chrono>
#include <thread>
#include "thirdparty/gnuplot-iostream.h"
#include <boost/program_options.hpp>

#include "Firelog.h"
#include "Matchbox.h"

namespace po = boost::program_options;

// }}}1

bool verbose = false;
bool log_provided = false;
bool cfg_provided = false;
bool plot = false;
bool plot_deltas = false;
bool timed = false;
bool read_cache = false;
bool plot_outliers = false;
bool plot_hough = false;
bool safe = true;

using namespace std::chrono;

int main(int argc, char** argv){

	std::string cfg_path, log_path;
	std::string plot_dir = "plots";
	float gaussian_deviation = 0.1;

// {{{2 Command line argument processing
	
	po::options_description desc("OPTIONS");
	desc.add_options()
		("help,h", "Show this help message and exit")
		("verbose,v", "Run in verbose mode")
		("log,L", po::value<std::string>(&log_path), "Log file path")
		("cfg,C", po::value<std::string>(&cfg_path), "Configuration file")
		("gauss-dev,g", po::value<float>(&gaussian_deviation), "Standard Gaussian deviation (metres)")
		("plot-path,p", po::value<std::string>(&plot_dir), "Plot directory path")
		("plot-deltas,D", "Plot average deltas")
		("plot-outliers,O", "Plot outlier scans")
		("plot-hough,H", "Plot hough spectrums")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	po::positional_options_description p;
	p.add("log", -1);
	po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
	po::notify(vm);

	if (argc < 2 || vm.count("help")) {
		std::cout << "Usage: ARSonist [options] [log file]\n" << desc << "\n";
		return 0;
	}
	if (vm.count("verbose")){
		verbose = true;
	}
	if(vm.count("cfg")){
		cfg_path = true;
		safe = false;
	}
	if(vm.count("plot-deltas")){
		plot_deltas = true;
	}
	if(vm.count("plot-outliers")){
		plot_outliers = true;
	}
	if(vm.count("plot-hough")){
		plot_outliers = true;
	}
	if(vm.count("cfg")){
		cfg_path = true;
		safe = false;
	}

	std::stringstream delline;
	delline << "\r                                                                                                                                                                                                                               " << std::flush;
// }}}2

	std::string cache_path = log_path;

	if(matchbox::exists(cache_path.append(".cache"))){
		std::cout << "Cache file found!\n";
		read_cache = true;
	}

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	firelog::Firelog log(cfg_path, log_path, verbose, safe);
	std::vector<matchbox::match> matches(log.messes.size());
	std::cout << "\nReading scan data..." << std::endl;
	std::fstream cache;
	if(read_cache) cache.open(cache_path, std::fstream::in);
	else cache.open(cache_path, std::fstream::out);

	std::cout << delline.str() << "\rComputing ARS..." << std::endl;
	if(verbose) std::cout << std::endl;

#pragma omp parallel for
	for (int i = 0; i < log.messes.size(); ++i) {
		if(verbose) std::cout << "\r                                                                                                                            \rProcessing scan "
			<< i << " with " << log.messes[i].scan.points.size() << " points in pose " << std::setprecision(3) << std::fixed << log.messes[i].wTb.transpose() << " [x y theta]" << std::flush; 
		
		std::vector<double> ars_feat;
		if(!read_cache){
			ars::AngularRadonSpectrum2d ars_obj;
			ars_obj.setARSFOrder(20);
			ars_obj.initLUT(0.0001);
			ars_obj.setComputeMode(ars::AngularRadonSpectrum2d::PNEBI_LUT);
			ars_obj.insertIsotropicGaussians(log.scans_points[i], gaussian_deviation);	//slow if done multiple times on the same object...?

			ars_feat = ars_obj.coefficients();
		}
		matchbox::makeMatch(ars_feat, log.messes[i].wTb.transpose()[0], log.messes[i].wTb.transpose()[1], log.messes[i].wTb.transpose()[2], log.scans_points[i], matches[i]);
	}
	
	std::cout << "\nCache operations...\n";
	for(matchbox::match& m : matches){
		std::cout << "\r." << std::flush;
		if(read_cache) firelog::readCache(m.ars_feature, cache);
		else firelog::writeCache(m.ars_feature, cache);
		std::cout << "\r " << std::flush;
	}
	cache.close();
	
	std::cout << "\nComputing accuracy...\n";
	matchbox::Matchbox mb(verbose);
	mb.addMatches(matches);
	mb.computeAccuracy();

	std::cout << delline.str() << "\rDone.\n";

// {{{3 Plot data
	if(plot_deltas){
		boost::filesystem::create_directories(plot_dir);
		std::string delta_plot_file = plot_dir;

		mb.plotDelta(23040, 2160, delta_plot_file.append("delta.png"));
	}

	if(plot_outliers){
		boost::filesystem::create_directories(plot_dir);
		if(verbose) std::cout << delline.str() << "Generating laser scan plots for outliers...\n";
		std::string outlier_plot_dir = plot_dir;
		boost::filesystem::create_directories(outlier_plot_dir.append("/outliers"));

		mb.plotOutliers(1280, THRESHOLD,  outlier_plot_dir);
	}

	if(plot_hough){
		boost::filesystem::create_directories(plot_dir);
		if(verbose) std::cout << delline.str() << "Generating Hough spectrum plots for points...\n";
		std::string hough_plot_dir = plot_dir;
		boost::filesystem::create_directories(hough_plot_dir.append("/hough"));

		mb.plotHough(1280, 1024, hough_plot_dir);
	}

	if(verbose) std::cout << delline.str() << "Plotting done.\n" << std::flush;
// }}}3

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duration = duration_cast<seconds>(t2-t1).count();

	if(timed) std::cout << "Execution time: " << duration << " seconds\n";

	return 0;
}

/** ARSonist
 *                                                )
 *    ,%,                                     ) _(___[]_
 *    %%%,&&&,                     ,%%,      (;`       /\
 *    %Y/%&&&&                     %%%%   ___/_____)__/ _\__     ,%%,
 *  ^^^||^&\Y&^^^^^^^^^^^^^^^^^^^^^%Y/%^^/ (_()   (  | /____/\^^^%%%%^^
 *    `    || _,..=xxxxxxxxxxxx,    ||   |(' |LI (.)I| | LI ||   %\Y%
 *   -=      /L_Y.-"""""""""`,-n-. `    @'---|__||___|_|____||_   ||
 *  ___-=___.--'[========]|L]J: []\ __________@//@___________) )______
 * -= _ _ _ |/ _ ''_ " " ||[ -_ 4 |  _  _  _  _  _  _  _  _  _  _  _
 *          '-(_)-(_)----'v'-(_)--'
 * --------------------------------------------------------------------
 *
 */

#ifndef FIRELOG_H
#define FIRELOG_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <Eigen/Dense>
#include <iomanip>
#include <ars/definitions.h>
#include <boost/algorithm/string/classification.hpp> // Include for boost::is_any_of
#include <boost/algorithm/string/split.hpp> // Include for boost::split

#include "NAV350_Message_V2.h"
#include "Point.h"
#include "LaserScan.h"
#include "ParamMap.h"
#include "CarmenV2Reader.h"
#include "Rate.h"

namespace firelog {
	
    const int MESSAGE_TYPE_NUM = 2;

    enum MessageType {
        MESS_NAV350 = 0, MESS_R2000 = 1
    };
    const char MESSAGE_TYPE_NAMES[][30] = { "MESS_NAV350", "MESS_R2000" };
    const int MESSAGE_SIZE[] = {24411, 31939};
    const int MESSAGE_SCAN_NUM_BEAMS[] = {1440, 3600};
    const double MESSAGE_SCAN_ANGLE_INC_DEG[] = {0.25, 0.1};
	/*
	 * Caching functionality for ARS spectrums
	 */
	void writeCache(std::vector<double> ars, std::fstream& cache);
	void readCache(std::vector<double>& ars, std::fstream& cache);
    void extractFromMsgAgvPose(const AGV_Message_V2& mess, Eigen::Vector3d& wTv, Eigen::Vector3d& vTl);
    void extractFromMsgLaserScan(const AGV_Message_V2& mess, int messType, int scanNumBeams, double scanAngleMin, double scanAngleInc, double scanSickOffset, LaserScan& scan);
	/*
	 * Class with utilities for log data extraction and manipulation
	 */
	class Firelog {
		public:
			std::vector<ars::Vector2Vector> scans_points;
			std::vector<RobotLaserMess> messes;

			Firelog();
			Firelog(std::string cfg_path, std::string log_path, bool v, bool safe);
			void setNum(int num = 180);
			void setAngles(double angleMin = -0.5*M_PI, double angleResi = M_PI/180*1.0);
			void setVerbose(bool v = true);
			void setMaxRange(double r);

		private:
			ParamMap params;
			int num = 180;
			double angleMin = -0.5*M_PI;
			double angleRes = M_PI/180*1.0;
			double maxRange = 40;
			bool verbose = false;
			bool safe_params = false;	//on some systems, parammap causes a segfault. when this is set to true, parammap is not used and safe defaults are assumed.

			void initE80(std::string cfg_path, std::string log_path, bool v);
			void rangeToPoint(std::vector<double>& ranges, ars::Vector2Vector& points, double& max_range);
			void getPoints(std::vector<double>& ranges);
	};

}

#endif

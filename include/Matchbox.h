/** ARSonist
 *                                                )
 *    ,%,                                     ) _(___[]_
 *    %%%,&&&,                     ,%%,      (;`       /\
 *    %Y/%&&&&                     %%%%   ___/_____)__/ _\__     ,%%,
 *  ^^^||^&\Y&^^^^^^^^^^^^^^^^^^^^^%Y/%^^/ (_()   (  | /____/\^^^%%%%^^
 *    `    || _,..=xxxxxxxxxxxx,    ||   |(' |LI (.)I| | LI ||   %\Y%
 *   -=      /L_Y.-"""""""""`,-n-. `    @'---|__||___|_|____||_   ||
 *  ___-=___.--'[========]|L]J: []\ __________@//@___________) )______
 * -= _ _ _ |/ _ ''_ " " ||[ -_ 4 |  _  _  _  _  _  _  _  _  _  _  _
 *          '-(_)-(_)----'v'-(_)--'
 * --------------------------------------------------------------------
 *
 */

#ifndef MATCHBOX_H
#define MATCHBOX_H
#include <iostream>
#include <iomanip>
#include <cmath>
#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <ars/definitions.h>
#include <ars/ars2d.h>
#include <ars/BBOptimizer1d.h>
#include <ars/HoughSpectrum.h>
#include "thirdparty/gnuplot-iostream.h"
#include <sys/stat.h>
#include <boost/filesystem.hpp>
#include <omp.h>
#include <chrono>
#include <thread>
#define PRINT_DIM(X) std::cout << #X << " rows " << X.rows() << " cols " << X.cols() << std::endl;
#define RAD2DEG(X) (180.0/M_PI*(X))
#define THRESHOLD 5.0
#define MAX_NEIGHBOURS 2

struct BoundInterval {
    double x0;
    double x1;
    double y0;
    double y1;
};

namespace matchbox {
	bool exists (const std::string& name);

	/**
	 * Contains the ars representation of a point and
	 * the corresponding reference angle
	 */
	typedef struct match {
		std::vector<double> ars_feature;	//ars coefficient vector
		double ars_angle = 0;	//ars maximum coefficient angle
		double x,y;	//position and reference angle
		double theta_reference = 0;
		double theta_reference_raw = 0;
		double theta_reference_delta = 0;
		ars::Vector2Vector points;
		std::vector<match *> neighbours;	//readings close enough to warrant comparison
		std::vector<double> correlation_deltas;
		double delta = 0;
		double corr_delta_avg;
		bool outlier = false;
	}match;

	double mod180(double angle);
	double subAngles(double a, double b);
	void makeMatch(std::vector<double> ars_feature, double x, double y, double theta_reference, ars::Vector2Vector points, matchbox::match& out);

	class Matchbox {
		public:
			Matchbox();
			Matchbox(bool v);
			void addMatches(std::vector<match>& new_matches);
			void computeAccuracy();
			void plotDelta(int width, int height, std::string out_file);
			void plotOutliers(int width, double threshold, std::string out_dir);
			void plotHough(int width, int height, std::string out_dir);

		private:
			std::vector<match *> matches;
			std::vector<double> delta;
			std::vector<double> delta_percent;
			bool verbose = false;
			int tested = 0;
			int outliers = 0;
			int corr_count = 0;

			void computeAngle(match* m);
			void setVerbose(bool v = true);

	};
}

#endif

/* 
 * File:   NAV350_Message_V2.h
 * Author: fab
 *
 * Created on August 31, 2015, 14:00
 */

#pragma pack(push, 1)
#pragma once

#define POLY 0x1021

struct Timestamps {
	uint32_t scan_timestamp;
	uint16_t nav_timestamp;
	uint32_t agv_epoch_time;
	uint16_t agv_epoch_millisec;
};

struct Parameters {
	double drive_scale;
	double steer_scale;
	double steer_offset;
	int16_t wheelbase;
	int16_t scan_x;
	int16_t scan_y;
	int16_t scan_t;
};

struct Odometry {
	uint32_t agv_epoch_time;
	uint16_t agv_epoch_millisec;
	uint16_t nav_timestamp;
	double steering_angle;
	double wheel_speed;
	int16_t wheel_encoder_tick;
	int32_t steer_encoder_tick;
	double delta_x;
	double delta_y;
	double delta_h;
};

struct Reflector {
	uint16_t timediff;
	float x;
	float y;
	float x_mod;
	float y_mod;
};

struct Point {
	uint32_t rho;
	uint16_t echo;
};

struct AHRSData {
	uint32_t agv_epoch_time;
	uint16_t agv_epoch_millisec;
	uint16_t nav_timestamp;
	double Wx;
	double Wy;
	double Wz;
	double Ax;
	double Ay;
	double Az;
	double Mx;
	double My;
	double Mz;
	double temperature;
	double pressure;
	double qw;
	double qx;
	double qy;
	double qz;
};

struct AHRSSensor {
	uint8_t ID;
	uint8_t data_number;
	AHRSData data[40];
};

struct AGV_Message_V2 {

	AGV_Message_V2() {
		header[0] = 65;
		header[1] = 78;
		header[2] = 65;
		header[3] = 86;
	}

	uint8_t header[4];
	uint32_t message_lenght;
	uint8_t protocol_version;
	uint32_t message_counter;
	Timestamps time;
	Parameters param;
	double agv_x;
	double agv_y;
	double agv_h;
	double sensor_x;
	double sensor_y;
	double sensor_h;
	double omega;
	double vx_agv;
	double vy_agv;
	int32_t scan_start_angle;
	int16_t sensor_height;
	uint8_t num_odometries;
	Odometry odom[40];
	uint8_t nav_data_ok;
	uint8_t num_reflectors;
	uint32_t ref_agv_epoch_time;
	uint16_t ref_agv_epoch_millisec;
	Reflector refl[28];
	Point points[3600]; /*Point points[1440];*/     /*R2000 points*/
	uint8_t num_ahrs_sensors;
	/*AHRSSensor ahrs[4];*/      /*removed, not interested in ahrs data*/
	uint16_t crc16;
}; //__attribute__((packed));

struct Checker {

	static bool checkMessageIntegrity(const AGV_Message_V2& mess) {
		uint16_t crc = crc16((uint8_t*) (&mess), 31937);
		return (mess.crc16 == crc);
	}

	static unsigned short crc16(uint8_t *data_p, unsigned short length) {
		unsigned int crc;

		crc = 0x0;

		if (length == 0)
			return (~crc);

		for (int j = 0; j < length; ++j) {
			uint8_t c = data_p[j];
			for (int i = 0; i < 8; i++) {
				bool bit = (((c >> (7 - i)) & 1) == 1);
				bool c15 = (((crc >> 15) & 1) == 1);
				crc <<= 1;
				if (c15 ^ bit) crc ^= POLY;
			}
		}

		crc &= 0xffff;

		return (crc);
	}
};
